$(document).ready(function(){
    $('.imgCarouselContent').slick({
        prevArrow: $('.imgCarouselBtnContent .prev'),
        nextArrow: $('.imgCarouselBtnContent .next'),
    });

    $('.feedSliderContent').slick({
        prevArrow: $('.imgFeedBtnContent .prev'),
        nextArrow: $('.imgFeedBtnContent .next'),
    });
  });
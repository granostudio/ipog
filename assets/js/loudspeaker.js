let loudChoiceBtn = $('.loudspeakerPostMenuItem');


// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < loudChoiceBtn.length; i++) {
  loudChoiceBtn[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("activeLoud");
    current[0].className = current[0].className.replace(" activeLoud", "");
    this.className += " activeLoud";
  });
}

jQuery(function(){
  //   jQuery('#showall').click(function(){
  //         jQuery('.targetDiv').show();
  //  });
   jQuery('.loudspeakerPostMenuItem').click(function(){
          jQuery('.loudspeakerPostBlockItem').hide();
          jQuery('#loudspeakerPost'+$(this).attr('target')).show();
   });
  });

let loudspeakerBtn = $('.menu-btn');
loudspeakerBtn.click(function(){
  loudspeakerBtn.toggleClass('loudActive');
  if (loudspeakerBtn.hasClass('loudActive')) {
    $('body').css('overflow','hidden');
  }
  else {
    $('body').css('overflow','inherit');
  }
});
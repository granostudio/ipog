let choiceBtn = $('.listInfoFrameItem');


// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < choiceBtn.length; i++) {
  choiceBtn[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}

jQuery(function(){
  //   jQuery('#showall').click(function(){
  //         jQuery('.targetDiv').show();
  //  });
   jQuery('.listInfoFrameItem').click(function(){
          jQuery('.infoItemBlock').hide();
          jQuery('#infoItem'+$(this).attr('target')).show();
          $('.tourSlide').slick('setPosition'); 
   });
  });